package org.bitbucket.openkilda.topo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BasicTopoTests.class })

public class AllTopoTests {

}
