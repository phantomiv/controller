Feature: Topology Discovery TrueUp

Scenario: Complicated Topology Setup, Validate Accuracy
  Given TrueUpGivenTBD
  When TrueUpWhenTBD
  Then TrueUpThenTBD
