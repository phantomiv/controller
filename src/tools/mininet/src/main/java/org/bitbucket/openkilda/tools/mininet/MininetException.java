package org.bitbucket.openkilda.tools.mininet;

/**
 * The Class MininetException.
 */
public class MininetException extends Exception {
  
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 1L;

  /**
   * Instantiates a new MininetException.
   *
   * @param message the message
   */
  public MininetException(String message) {
    super(message);
  }
}
